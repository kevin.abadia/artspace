package dev.codeninja.artspace

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dev.codeninja.artspace.ui.theme.ArtSpaceTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArtSpaceTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ArtSpaceScreen()
                }
            }
        }
    }
}

@Composable
fun ArtSpaceScreen(modifier: Modifier = Modifier) {

    var currentArtwork by remember {
        mutableStateOf(1)
    }

    data class Artwork(
        val currentArtwork: Int,
        val titleResId: Int,
        val descriptionResId: Int,
        val dateResId: Int
    )

    fun generateArtworkList(): List<Artwork> {
        val drawableIds = intArrayOf(
            R.drawable.sky_first_day,
            R.drawable.arabella_on_the_street,
            R.drawable.baby_shining_mia,
            R.drawable.motorcyclist_cora,
            R.drawable.heights_zoe,
            R.drawable.night_mode_mono,
            R.drawable.cora_in_love,
            R.drawable.shining_mia,
            R.drawable.sky_hugging_arabella,
            R.drawable.winter_mono,
            R.drawable.sky_in_the_rain,
            R.drawable.zoe_waiting_lunch
        )

        val titleIds = intArrayOf(
            R.string.sky_first_day,
            R.string.arabella_on_the_street,
            R.string.baby_shining_mia,
            R.string.motorcyclist_cora,
            R.string.heights_zoe,
            R.string.night_mode_mono,
            R.string.cora_in_love,
            R.string.shining_mia,
            R.string.sky_hugging_arabella,
            R.string.winter_mono,
            R.string.sky_in_the_rain,
            R.string.zoe_waiting_lunch
        )

        val descriptionIds = intArrayOf(
            R.string.sky_first_day_description,
            R.string.arabella_description,
            R.string.baby_shining_mia_description,
            R.string.motorcyclist_cora_description,
            R.string.heights_zoe_description,
            R.string.night_mode_mono_description,
            R.string.cora_in_love_description,
            R.string.shining_mia_description,
            R.string.sky_hugging_arabella_description,
            R.string.winter_mono_description,
            R.string.sky_in_the_rain_description,
            R.string.zoe_waiting_lunch_description
        )

        val dateIds = intArrayOf(
            R.string.sky_first_day_date,
            R.string.arabella_date,
            R.string.baby_shining_mia_date,
            R.string.motorcyclist_cora_date,
            R.string.heights_zoe_date,
            R.string.night_mode_mono_date,
            R.string.cora_in_love_date,
            R.string.shining_mia_date,
            R.string.sky_hugging_arabella_date,
            R.string.winter_mono_date,
            R.string.sky_in_the_rain_date,
            R.string.zoe_waiting_lunch_date
        )

        val artworkList = mutableListOf<Artwork>()
        for (i in drawableIds.indices) {
            val artwork = Artwork(
                currentArtwork = drawableIds[i],
                titleResId = titleIds[i],
                descriptionResId = descriptionIds[i],
                dateResId = dateIds[i]
            )
            artworkList.add(artwork)
        }

        return artworkList
    }

    val artworkList = generateArtworkList()

    Column(
        modifier = modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        val index = currentArtwork - 1
        val artwork = artworkList.getOrNull(index)

        Spacer(modifier = modifier.size(24.dp))

        Box(
            modifier = Modifier
                .background(
                    color = colorResource(id = R.color.paynes_gray),
                    shape = RoundedCornerShape(16.dp)
                )
                .padding(16.dp)
        ) {
            Text(
                stringResource(R.string.student_name_and_code),
                color = colorResource(id = R.color.platinum)
            )
        }

        Spacer(modifier = modifier.size(24.dp))

        if (artwork != null) {
            ImageWithDescriptionAndTitle(
                currentArtwork = artwork.currentArtwork,
                title = artwork.titleResId,
                description = artwork.descriptionResId,
                date = artwork.dateResId
            )
        }

        Spacer(modifier = modifier.size(25.dp))

        NavigationButtons(
            onIncrementClick = {
                if (currentArtwork != artworkList.size) {
                    currentArtwork++
                } else {
                    currentArtwork = 1
                }
            },
            onDecrementClick = {
                if (currentArtwork > 1) {
                    currentArtwork--
                } else {
                    currentArtwork = artworkList.size
                }
            },
            setFirstElement = { currentArtwork = 1 }
        )
    }
}


@Composable
fun ImageWithDescriptionAndTitle(
    modifier: Modifier = Modifier,
    @DrawableRes currentArtwork: Int, @StringRes title: Int,
    @StringRes description: Int,
    @StringRes date: Int
) {


    Image(
        painter = painterResource(currentArtwork),
        contentDescription = stringResource(id = description),
        modifier = modifier
            .fillMaxWidth()
            .padding(12.dp)
            .clip(RoundedCornerShape(16.dp)),
        contentScale = ContentScale.FillWidth
    )

    Spacer(modifier = modifier.size(16.dp))

    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        // Artwork title
        Text(
            text = stringResource(id = title),
            fontWeight = FontWeight.Bold,
            color = colorResource(id = R.color.blue_100),
            fontSize = 32.sp
        )

        // Artwork year
        Text(
            text = "— ${stringResource(id = date)} —",
            fontSize = 16.sp,
            fontWeight = FontWeight.Medium,
            color = colorResource(id = R.color.gray_300)
        )

        Text(
            text = stringResource(id = description),
            fontSize = 16.sp,
            fontWeight = FontWeight.Medium,
            color = colorResource(id = R.color.onyx)
        )
    }

}

@Composable
fun NavigationButtons(
    modifier: Modifier = Modifier,
    onIncrementClick: () -> Unit,
    onDecrementClick: () -> Unit,
    setFirstElement: () -> Unit,
) {
    Row(
        modifier = modifier.padding(horizontal = 8.dp),
        horizontalArrangement = Arrangement.spacedBy(
            8.dp,
            Alignment.CenterHorizontally
        )
    ) {
        // Previous Button
        Button(
            onClick = {
                onDecrementClick()
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = colorResource(id = R.color.paynes_gray)
            ),
            elevation = ButtonDefaults.elevation(
                defaultElevation = 1.dp,
                pressedElevation = 0.dp,
                focusedElevation = 0.dp
            )
        ) {
            Text(
                text = stringResource(id = R.string.prev_btn_text),
                fontSize = 16.sp,
                fontWeight = FontWeight.Medium,
                color = colorResource(id = R.color.teal_200)
            )
        }

        // Next Button
        Button(
            onClick = {
                onIncrementClick()
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = colorResource(id = R.color.cadet_gray)
            ),
            elevation = ButtonDefaults.elevation(
                defaultElevation = 1.dp,
                pressedElevation = 0.dp,
                focusedElevation = 0.dp
            )
        ) {
            Text(
                text = stringResource(id = R.string.next_btn_text),
                fontSize = 16.sp,
                fontWeight = FontWeight.Medium,
                color = colorResource(id = R.color.platinum)
            )
        }

        Icon(
            imageVector = Icons.Default.Refresh,
            contentDescription = stringResource(id = R.string.refresh),
            modifier = Modifier
                .clickable { setFirstElement() }
                .align(Alignment.CenterVertically)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ArtSpaceTheme {
        ArtSpaceScreen()
    }
}